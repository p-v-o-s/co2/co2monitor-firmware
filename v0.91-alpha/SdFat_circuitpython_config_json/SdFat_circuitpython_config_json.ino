// Adafruit M0 Express CircuitPython Flash Example
// Author: Tony DiCola
//
// This is an example of reading and writing data from Arduino
// to the M0 Express flash filesystem used by CircuitPython.
// You can create, update, and read files on the CircuitPython
// filesystem in an Arduino sketch and then later load CircuitPython
// to interact with the same files.  This example will print out
// the contents of boot.py and main.py (if found) and add a line
// to a data.txt file on CircuitPython's filesystem.
//
// Note before you use this sketch you must load CircuitPython
// on your M0 Express.  This will create the filesystem and
// initialize it, then you can load this example and read/write
// files on the board.
//
// Usage:
// - Modify the pins and type of fatfs object in the config
//   section below if necessary (usually not necessary).
// - Upload this sketch to your M0 express board.
// - Open the serial monitor at 115200 baud.  You should see the
//   example start to run and messages printed to the monitor.
//   If you don't see anything close the serial monitor, press
//   the board reset buttton, wait a few seconds, then open the
//   serial monitor again.
#include <SPI.h>
#include <SdFat.h>
#include <Adafruit_SPIFlash.h>
#include <ArduinoJson.h>

// On-board external flash (QSPI or SPI) macros should already
// defined in your board variant if supported
// - EXTERNAL_FLASH_USE_QSPI
// - EXTERNAL_FLASH_USE_CS/EXTERNAL_FLASH_USE_SPI
#if defined(EXTERNAL_FLASH_USE_QSPI)
  Adafruit_FlashTransport_QSPI flashTransport;

#elif defined(EXTERNAL_FLASH_USE_SPI)
  Adafruit_FlashTransport_SPI flashTransport(EXTERNAL_FLASH_USE_CS, EXTERNAL_FLASH_USE_SPI);

#else
  #error No QSPI/SPI flash are defined on your board variant.h !
#endif

Adafruit_SPIFlash flash(&flashTransport);

// file system object from SdFat
FatFileSystem fatfs;

const char configfile[] = "config.json";

struct Config {
  char hostname[64];
  int port;
};

Config config;   

void setup() {

  pinMode(LED_BUILTIN, OUTPUT);
  
  // Initialize serial port and wait for it to open before continuing.
  Serial.begin(115200);
  while (!Serial) {
    delay(100);
  }
  Serial.println("Adafruit M0 Express CircuitPython Flash Example");

  // Initialize flash library and check its chip ID.
  if (!flash.begin()) {
    Serial.println("Error, failed to initialize flash chip!");
    while(1);
  }
  Serial.print("Flash chip JEDEC ID: 0x"); Serial.println(flash.getJEDECID(), HEX);

  // First call begin to mount the filesystem.  Check that it returns true
  // to make sure the filesystem was mounted.
  if (!fatfs.begin(&flash)) {
    Serial.println("Failed to mount filesystem!");
    Serial.println("Was CircuitPython loaded on the board first to create the filesystem?");
    while(1);
  }
  Serial.println("Mounted filesystem!");

  // Check if a boot.py exists and print it out.
  if (fatfs.exists(configfile)) {
    File file = fatfs.open(configfile, FILE_READ);

StaticJsonDocument<512> doc;

DeserializationError error = deserializeJson(doc, file);
  if (error)
    Serial.println(F("Failed to read file, using default configuration"));

  // Copy values from the JsonDocument to the Config
  config.port = doc["port"] | 2731;
  strlcpy(config.hostname,                  // <- destination
          doc["hostname"] | "example.com",  // <- source
          sizeof(config.hostname));         // <- destination's capacity

  // Close the file (Curiously, File's destructor doesn't close the file)
  //fatfs.close();

  
    /*
    Serial.println("Printing config file...");
    while (bootPy.available()) {
      char c = bootPy.read();
      //int r = atoi(c);
      //int sum = r + 3;
      Serial.print(c);
      //Serial.print("Sum:");
      //Serial.println(sum);
    }
    Serial.println();
*/


    
  }
  else {
    Serial.println("No config file found...");
  }

Serial.println("got:");
Serial.println(config.port);
Serial.println(config.hostname);

}

void loop() {
  // Nothing to do in the loop.
   digitalWrite(LED_BUILTIN, HIGH);   // turn the LED on (HIGH is the voltage level)
  delay(1000);                       // wait for a second
  digitalWrite(LED_BUILTIN, LOW);    // turn the LED off by making the voltage LOW
  delay(1000);      
}
