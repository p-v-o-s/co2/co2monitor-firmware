
/*
  Web client

 This sketch connects to a website (wifitest.adafruit.com/testwifi/index.html)
 using the WiFi module.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 Circuit:
 * Board with NINA module (Arduino MKR WiFi 1010, MKR VIDOR 4000 and UNO WiFi Rev.2)

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */


#include <SPI.h>
#include "wiring_private.h" // pinPeripheral() function
#include <ArduinoHttpClient.h>
#include <WiFiNINA.h>
#include <ArduinoJson.h> //https://arduinojson.org/v6/doc/installation/
#include <SD.h>
#include <U8x8lib.h>
#include <Wire.h>
#include <Bounce2.h> // https://github.com/thomasfredericks/Bounce2
//#include <ArduinoHttpClient.h>
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
#include "arduino_secrets.h"

SCD30 airSensor;


#include <RH_RF95.h>
#include <RHSoftwareSPI.h>
#include <RHRouter.h>
#include <RHMesh.h>

#define LORA_IRQ A2
#define LORA_CS A4
#define LORA_RST A5

#define LORA_SCK 13 
#define LORA_MOSI 11
#define LORA_MISO 12 

#define LORA_FREQ 915.0

U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);

// set up variables using the SD utility library functions:
Sd2Card card;
SdVolume volume;
SdFile root;

// change this to match your SD shield or module;
// Arduino Ethernet shield: pin 4
// Adafruit SD shields and modules: pin 10
// Sparkfun SD shield: pin 8
// MKRZero SD: SDCARD_SS_PIN
const int chipSelect = A1;

// Configure the pins used for the ESP32 connection
#if defined(ADAFRUIT_FEATHER_M4_EXPRESS) || \
  defined(ADAFRUIT_FEATHER_M0_EXPRESS) || \
  defined(ADAFRUIT_FEATHER_M0) || \
  defined(ARDUINO_AVR_FEATHER32U4) || \
  defined(ARDUINO_NRF52840_FEATHER) || \
  defined(ADAFRUIT_ITSYBITSY_M0) || \
  defined(ADAFRUIT_ITSYBITSY_M4_EXPRESS) || \
  defined(ARDUINO_AVR_ITSYBITSY32U4_3V) || \
  defined(ARDUINO_NRF52_ITSYBITSY)
  // Configure the pins used for the ESP32 connection
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS    10   // Chip select pin
  #define ESP32_RESETN  7   // Reset pin
  #define SPIWIFI_ACK   9   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(ARDUINO_AVR_FEATHER328P)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS     4   // Chip select pin
  #define ESP32_RESETN   3   // Reset pin
  #define SPIWIFI_ACK    2   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(TEENSYDUINO)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS     5   // Chip select pin
  #define ESP32_RESETN   6   // Reset pin
  #define SPIWIFI_ACK    9   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(ARDUINO_NRF52832_FEATHER)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS    16   // Chip select pin
  #define ESP32_RESETN  15   // Reset pin
  #define SPIWIFI_ACK    7   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif !defined(SPIWIFI_SS)   // if the wifi definition isnt in the board variant
  // Don't change the names of these #define's! they match the variant ones
  #define SPIWIFI       SPI
  #define SPIWIFI_SS    10   // Chip select pin
  #define SPIWIFI_ACK    7   // a.k.a BUSY or READY pin
  #define ESP32_RESETN   5   // Reset pin
  #define ESP32_GPIO0   -1   // Not connected
#endif



SPIClass mySPI (&sercom1, 12, 13, 11, SPI_PAD_0_SCK_1, SERCOM_RX_PAD_3);
// do this first, for Reasons

RHSoftwareSPI sx1278_spi;
RH_RF95 rf95(LORA_CS, LORA_IRQ, sx1278_spi);

RHMesh *manager;


// buttons
#define BUTTON_A_PIN A3 
Button button_A = Button();

//WiFiClient wifi;
//HttpClient client = HttpClient(wifi, serverName, port);



///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = SECRET_SSID;        // your network SSID (name)
char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key Index number (needed only for WEP)

int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//IPAddress server(74,125,232,128);  // numeric IP for Google (no DNS)



// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

HttpClient http = HttpClient(client, server, 80);

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);
  
  /*while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  */

 button_A.attach( BUTTON_A_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_A.interval(5); 
  button_A.setPressedState(LOW);
  
mySPI.begin();


  u8x8.begin();
  
  u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("Starting...");
   delay(1000);

   Wire.begin();

  if (airSensor.begin() == false)
  {
     u8x8.setCursor(0,2); 
   u8x8.print("SCD30 missing?");
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }

   delay(1000);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("SCD30 works!");
   delay(1000);


   
// Assign pins 11, 12, 13 to SERCOM functionality
pinPeripheral(11, PIO_SERCOM);
pinPeripheral(12, PIO_SERCOM);
pinPeripheral(13, PIO_SERCOM);

// the pins for the virtual SPI explicitly to the internal connection
    sx1278_spi.setPins(LORA_MISO, LORA_MOSI, LORA_SCK);


    //lora

   // LoRa: Init
    pinMode(LORA_RST, OUTPUT);
    digitalWrite(LORA_RST, LOW);
    delay(100);
    digitalWrite(LORA_RST, HIGH);

    if (!rf95.init()) 
        Serial.println("LoRa Radio: init failed.");
    else {
        Serial.println("LoRa Radio: init OK!");
       
    }
    // LoRa: set frequency
    if (!rf95.setFrequency(LORA_FREQ))
        Serial.println("LoRa Radio: setFrequency failed.");
    else
        Serial.printf("LoRa Radio: freqency set to %f MHz\n", LORA_FREQ);

    rf95.setModemConfig(RH_RF95::Bw125Cr45Sf128);

    // LoRa: Set max (23 dbm) transmission power. 
    rf95.setTxPower(23, false);
    
     u8x8.setCursor(0,2); 
    u8x8.print("LoRa Works!");
      delay(1000);



    
    Serial.print("\nInitializing SD card...");

  // we'll use the initialization code from the utility libraries
  // since we're just testing if the card is working!
  if (!card.init(SPI_HALF_SPEED, chipSelect)) {
    Serial.println("initialization failed. Things to check:");
    Serial.println("* is a card inserted?");
    Serial.println("* is your wiring correct?");
    Serial.println("* did you change the chipSelect pin to match your shield or module?");
    //while (1);
    u8x8.setCursor(0,4); 
    u8x8.print("no uSD?");
      delay(1000);
  } else {
    Serial.println("Wiring is correct and a card is present.");
    u8x8.setCursor(0,4); 
    u8x8.print("usD Works!");
      delay(1000);
  }
  
  Serial.println("trying ...");
  // check for the WiFi module:
  WiFi.setPins(SPIWIFI_SS, SPIWIFI_ACK, ESP32_RESETN, ESP32_GPIO0, &SPIWIFI);
  while (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    u8x8.setCursor(0,6); 
    u8x8.print("wifi module?");
    delay(1000);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < "1.0.0") {
    Serial.println("Please upgrade the firmware");
  }
  Serial.print("Found firmware "); Serial.println(fv);

  u8x8.setCursor(0,6); 
    u8x8.print("wifi works!");
    delay(1000);
    
  // attempt to connect to Wifi network:
  Serial.print("Attempting to connect to SSID: ");
  Serial.println(ssid);
  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:
  
  /*do {
    status = WiFi.begin(ssid, pass);
    delay(100);     // wait until connection is ready!
  } while (status != WL_CONNECTED);
*/
  //Serial.println("Connected to wifi");
  //printWifiStatus();

/*
  Serial.println("\nStarting connection to server...");
  // if you get a connection, report back via serial:
  if (wifi.connect(server, 80)) {
    Serial.println("connected to server");
    // Make a HTTP request:
    wifi.print("GET "); wifi.print(path); wifi.println(" HTTP/1.1");
    wifi.print("Host: "); wifi.println(server);
    wifi.println("Connection: close");
    wifi.println();
  }
*/
  
}

int co2=1000;
float temperature=25.;
float humidity=50.;
float light=100.;

void loop() {

  button_A.update();

if ( button_A.pressed() ) {
  
  Serial.println("button A!");
delay(1000);

}

postToBayou();


}


void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void postToBayou() {

// Connect to wifi ...
Serial.print("Connecting to wifi ...");

while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to Network named: ");
    Serial.println(ssid);
    status = WiFi.begin(ssid, pass);
    delay(10000);
  }

Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  IPAddress ip = WiFi.localIP();
  IPAddress gateway = WiFi.gatewayIP();
  Serial.print("IP Address: ");
  Serial.println(ip);



  

char full_path [60];
strcpy(full_path, path);
strcat(full_path,this_node_pubkey);


//Form the JSON:
  DynamicJsonDocument doc(1024);
  
//  Serial.println(post_url);

  
  doc["private_key"] = this_node_privkey;
  doc["co2_ppm"] =  co2;
  doc["temperature_c"]=temperature;
  doc["humidity_rh"]=humidity;
  doc["node_id"]=2;
  doc["log"]="OK";
   
  String json;
  serializeJson(doc, json);
  serializeJson(doc, Serial);
  Serial.println("\n");

    String contentType = "application/json";

  http.post(full_path, contentType, json);

  // read the status code and body of the response
  int statusCode = http.responseStatusCode();
  Serial.print("Status code: ");
  Serial.println(statusCode);
  String response = http.responseBody();
  Serial.print("Response: ");
  Serial.println(response);

  
  delay(interval_sec*1000);
 
             
        
}
