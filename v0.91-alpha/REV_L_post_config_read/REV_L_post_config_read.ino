
/*
  Web client

 This sketch connects to a website (wifitest.adafruit.com/testwifi/index.html)
 using the WiFi module.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 This example is written for a network using WPA encryption. For
 WEP or WPA, change the Wifi.begin() call accordingly.

 Circuit:
 * Board with NINA module (Arduino MKR WiFi 1010, MKR VIDOR 4000 and UNO WiFi Rev.2)

 created 13 July 2010
 by dlf (Metodo2 srl)
 modified 31 May 2012
 by Tom Igoe
 */


#include <SPI.h>
#include "wiring_private.h" // pinPeripheral() function
#include <ArduinoHttpClient.h>
#include <WiFiNINA.h>
#include <ArduinoJson.h> //https://arduinojson.org/v6/doc/installation/
#include <SdFat.h>
#include <Adafruit_SPIFlash.h>
#include <U8x8lib.h>
#include <Wire.h>
#include <Bounce2.h> // https://github.com/thomasfredericks/Bounce2
//#include <ArduinoHttpClient.h>
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
#include "arduino_secrets.h"


SCD30 airSensor;


#include <RH_RF95.h>
#include <RHSoftwareSPI.h>
#include <RHRouter.h>
#include <RHMesh.h>

#define LORA_IRQ A2
#define LORA_CS A4
#define LORA_RST A5

#define LORA_SCK 13 
#define LORA_MOSI 11
#define LORA_MISO 12 

#define LORA_FREQ 915.0

U8X8_SSD1306_128X64_NONAME_HW_I2C u8x8(/* reset=*/ U8X8_PIN_NONE);


// change this to match your SD shield or module;
// Arduino Ethernet shield: pin 4
// Adafruit SD shields and modules: pin 10
// Sparkfun SD shield: pin 8
// MKRZero SD: SDCARD_SS_PIN
const int chipSelect = A1;

// Configure the pins used for the ESP32 connection
#if defined(ADAFRUIT_FEATHER_M4_EXPRESS) || \
  defined(ADAFRUIT_FEATHER_M0_EXPRESS) || \
  defined(ADAFRUIT_FEATHER_M0) || \
  defined(ARDUINO_AVR_FEATHER32U4) || \
  defined(ARDUINO_NRF52840_FEATHER) || \
  defined(ADAFRUIT_ITSYBITSY_M0) || \
  defined(ADAFRUIT_ITSYBITSY_M4_EXPRESS) || \
  defined(ARDUINO_AVR_ITSYBITSY32U4_3V) || \
  defined(ARDUINO_NRF52_ITSYBITSY)
  // Configure the pins used for the ESP32 connection
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS    10   // Chip select pin
  #define ESP32_RESETN  7   // Reset pin
  #define SPIWIFI_ACK   9   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(ARDUINO_AVR_FEATHER328P)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS     4   // Chip select pin
  #define ESP32_RESETN   3   // Reset pin
  #define SPIWIFI_ACK    2   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(TEENSYDUINO)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS     5   // Chip select pin
  #define ESP32_RESETN   6   // Reset pin
  #define SPIWIFI_ACK    9   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif defined(ARDUINO_NRF52832_FEATHER)
  #define SPIWIFI       SPI  // The SPI port
  #define SPIWIFI_SS    16   // Chip select pin
  #define ESP32_RESETN  15   // Reset pin
  #define SPIWIFI_ACK    7   // a.k.a BUSY or READY pin
  #define ESP32_GPIO0   -1
#elif !defined(SPIWIFI_SS)   // if the wifi definition isnt in the board variant
  // Don't change the names of these #define's! they match the variant ones
  #define SPIWIFI       SPI
  #define SPIWIFI_SS    10   // Chip select pin
  #define SPIWIFI_ACK    7   // a.k.a BUSY or READY pin
  #define ESP32_RESETN   5   // Reset pin
  #define ESP32_GPIO0   -1   // Not connected
#endif



SPIClass mySPI (&sercom1, 12, 13, 11, SPI_PAD_0_SCK_1, SERCOM_RX_PAD_3);
// do this first, for Reasons

RHSoftwareSPI sx1278_spi;
RH_RF95 rf95(LORA_CS, LORA_IRQ, sx1278_spi);

RHMesh *manager;


// buttons
#define BUTTON_A_PIN A3 
Button button_A = Button();

//WiFiClient wifi;
//HttpClient client = HttpClient(wifi, serverName, port);



///////please enter your sensitive data in the Secret tab/arduino_secrets.h
char ssid[] = SECRET_SSID;        // your network SSID (name)
char pass[] = SECRET_PASS;    // your network password (use for WPA, or use as key for WEP)
int keyIndex = 0;            // your network key Index number (needed only for WEP)

int status = WL_IDLE_STATUS;
// if you don't want to use DNS (and reduce your sketch size)
// use the numeric IP instead of the name for the server:
//IPAddress server(74,125,232,128);  // numeric IP for Google (no DNS)



// Initialize the Ethernet client library
// with the IP address and port of the server
// that you want to connect to (port 80 is default for HTTP):
WiFiClient client;

HttpClient http = HttpClient(client, server, 80);

#if defined(EXTERNAL_FLASH_USE_QSPI)
  Adafruit_FlashTransport_QSPI flashTransport;

#elif defined(EXTERNAL_FLASH_USE_SPI)
  Adafruit_FlashTransport_SPI flashTransport(EXTERNAL_FLASH_USE_CS, EXTERNAL_FLASH_USE_SPI);

#else
  #error No QSPI/SPI flash are defined on your board variant.h !
#endif

Adafruit_SPIFlash flash(&flashTransport);

// file system object from SdFat
FatFileSystem fatfs;

const char configfile[] = "config.json";

struct Config {
  char ssid[64];
  char pswd[64];
  char pubkey[64];
  char privkey[64];
  char server[64];
  char path[64];
  int interval_sec;
};

Config config;   

long measureDelay;

void setup() {
  //Initialize serial and wait for port to open:
  Serial.begin(9600);

  //Serial.println("FLASH_CS:");
  //Serial.println(EXTERNAL_FLASH_USE_CS);
  
  /*while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  }
  */

  
  digitalWrite(SPIWIFI_SS,HIGH);
  pinMode(SPIWIFI_SS,OUTPUT);
  
 button_A.attach( BUTTON_A_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_A.interval(5); 
  button_A.setPressedState(LOW);
  
mySPI.begin();


  u8x8.begin();
  
  u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("Starting...");
   delay(1000);

   Wire.begin();

  if (airSensor.begin() == false)
  {
     u8x8.setCursor(0,2); 
   u8x8.print("SCD30 missing?");
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }

   delay(1000);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("SCD30 works!");
   delay(1000);


   
// Assign pins 11, 12, 13 to SERCOM functionality
pinPeripheral(11, PIO_SERCOM);
pinPeripheral(12, PIO_SERCOM);
pinPeripheral(13, PIO_SERCOM);

// the pins for the virtual SPI explicitly to the internal connection
    sx1278_spi.setPins(LORA_MISO, LORA_MOSI, LORA_SCK);


    //lora

   // LoRa: Init
    pinMode(LORA_RST, OUTPUT);
    digitalWrite(LORA_RST, LOW);
    delay(100);
    digitalWrite(LORA_RST, HIGH);

    if (!rf95.init()) 
        Serial.println("LoRa Radio: init failed.");
    else {
        Serial.println("LoRa Radio: init OK!");
       
    }
    // LoRa: set frequency
    if (!rf95.setFrequency(LORA_FREQ))
        Serial.println("LoRa Radio: setFrequency failed.");
    else
        Serial.printf("LoRa Radio: freqency set to %f MHz\n", LORA_FREQ);

    rf95.setModemConfig(RH_RF95::Bw125Cr45Sf128);

    // LoRa: Set max (23 dbm) transmission power. 
    rf95.setTxPower(23, false);
    
     u8x8.setCursor(0,2); 
    u8x8.print("LoRa Works!");
      delay(1000);


// Initialize flash library and check its chip ID.
  if (!flash.begin()) {
    Serial.println("Error, failed to initialize flash chip!");
    while(1);
  }
  Serial.print("Flash chip JEDEC ID: 0x"); Serial.println(flash.getJEDECID(), HEX);

  // First call begin to mount the filesystem.  Check that it returns true
  // to make sure the filesystem was mounted.
  if (!fatfs.begin(&flash)) {
    Serial.println("Failed to mount filesystem!");
    Serial.println("Was CircuitPython loaded on the board first to create the filesystem?");
    while(1);
  }
  Serial.println("Mounted filesystem!");

  // Check if a boot.py exists and print it out.
  if (fatfs.exists(configfile)) {
    File file = fatfs.open(configfile, FILE_READ);

StaticJsonDocument<512> doc;

DeserializationError error = deserializeJson(doc, file);
  if (error)
    Serial.println(F("Failed to read file, using default configuration"));


  // Copy values from the JsonDocument to the Config
  config.interval_sec = doc["interval_seconds"] | 3;
  
  strlcpy(config.ssid,                  // <- destination
          doc["wifi_ssid"] ,  // <- source
          sizeof(config.ssid));         // <- destination's capacity

  strlcpy(config.pswd,                  // <- destination
          doc["wifi_password"] ,  // <- source
          sizeof(config.pswd));         // <- destination's capacity     

             
  strlcpy(config.pubkey,                  // <- destination
          doc["public_key"] ,  // <- source
          sizeof(config.pubkey));         // <- destination's capacity 

  strlcpy(config.privkey,                  // <- destination
        doc["private_key"] ,  // <- source
        sizeof(config.privkey));         // <- destination's capacity 

  strlcpy(config.server,                  // <- destination
      doc["server"] ,  // <- source
      sizeof(config.server));         // <- destination's capacity 


  strlcpy(config.path,                  // <- destination
      doc["path"] ,  // <- source
      sizeof(config.path));         // <- destination's capacity 
      
  config.interval_sec = doc["interval_seconds"];

  Serial.print("interval:");
  Serial.println(config.interval_sec);
  // Close the file (Curiously, File's destructor doesn't close the file)
  //fatfs.close();

  
    /*
    Serial.println("Printing config file...");
    while (bootPy.available()) {
      char c = bootPy.read();
      //int r = atoi(c);
      //int sum = r + 3;
      Serial.print(c);
      //Serial.print("Sum:");
      //Serial.println(sum);
    }
    Serial.println();
*/


    
  }
  else {
    Serial.println("No config file found...");
  }

//Serial.println("got:");
//Serial.println(config.port);
//Serial.println(config.hostname);
    
//
//SPI.endTransaction();

 //digitalWrite(SPIWIFI_SS,LOW);
 // pinMode(SPIWIFI_SS,OUTPUT);
  
delay(1000);
  
  Serial.println("trying ...");
  // check for the WiFi module:
  WiFi.setPins(SPIWIFI_SS, SPIWIFI_ACK, ESP32_RESETN, ESP32_GPIO0, &SPIWIFI);
  
  while (WiFi.status() == WL_NO_MODULE) {
    Serial.println("Communication with WiFi module failed!");
    // don't continue
    u8x8.setCursor(0,6); 
    u8x8.print("wifi module?");
    delay(1000);
  }

  String fv = WiFi.firmwareVersion();
  if (fv < "1.0.0") {
    Serial.println("Please upgrade the firmware");
  }
  Serial.print("Found firmware "); Serial.println(fv);

  u8x8.setCursor(0,6); 
    u8x8.print("wifi works!");
    delay(1000);
    
  // attempt to connect to Wifi network:
  Serial.print("Attempting to connect to SSID: ");
  Serial.println(config.ssid);
  // Connect to WPA/WPA2 network. Change this line if using open or WEP network:


  measureDelay=config.interval_sec*1000;
  
}

int co2=1000;
float temperature=25.;
float humidity=50.;
float light=100.;

long lastMeasureTime = 0;  // the last time the output pin was toggled


//long measureDelay = config.interval_sec*1000;
//long measureDelay = 5*1000;


int firstLoop = 1;

void loop() {

  button_A.update();

if ( button_A.pressed() ) {
  
  Serial.println("button A!");
delay(1000);

}


if (  ( (millis() - lastMeasureTime) > measureDelay) || firstLoop) {

 

 if (airSensor.dataAvailable())
  {

    if (firstLoop) firstLoop = 0;

u8x8.setFont(u8x8_font_chroma48medium8_r);
  u8x8.setCursor(10,2);
  u8x8.print("...");
  
  co2 = airSensor.getCO2();
    temperature = roundf(airSensor.getTemperature()* 100) / 100;
    humidity = roundf(airSensor.getHumidity()* 100) / 100;

    u8x8.clear();
    //u8x8.setFont(u8x8_font_7x14B_1x2_f);
    //u8x8.setFont(u8x8_font_inr33_3x6_f);
    u8x8.setFont(u8x8_font_inb21_2x4_n);
    u8x8.setCursor(0,0); 
    u8x8.print(co2);
    
    u8x8.setFont(u8x8_font_7x14B_1x2_f);
    u8x8.setCursor(0,4); 
    u8x8.print(temperature);
    u8x8.print(" C");
    u8x8.setCursor(0,6); 
    u8x8.print(humidity);
    u8x8.print(" RH");
    
    postToBayou();

  }

    lastMeasureTime = millis(); //set the current time
    
}




}


void printWifiStatus() {
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(WiFi.SSID());

  // print your board's IP address:
  IPAddress ip = WiFi.localIP();
  Serial.print("IP Address: ");
  Serial.println(ip);

  // print the received signal strength:
  long rssi = WiFi.RSSI();
  Serial.print("signal strength (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}

void postToBayou() {

// Connect to wifi ...
Serial.print("Connecting to wifi ...");


  
while (status != WL_CONNECTED) {
    Serial.print("Attempting to connect to Network named: ");
    Serial.println(ssid);
  
    u8x8.setCursor(10,2);
      u8x8.print("///");
      delay(300);
      u8x8.setCursor(10,2);
      u8x8.print("---");
      delay(300);
      u8x8.setCursor(10,2);
      u8x8.print("\\\ ");
      delay(300);
      u8x8.setCursor(10,2);
      u8x8.print("|||");
      delay(300);
       u8x8.setCursor(10,2);
      u8x8.print("///");
      delay(300);
      u8x8.setCursor(10,2);
      u8x8.print("---");
      delay(300);
   
    status = WiFi.begin(config.ssid, config.pswd);
   
  }

Serial.print("SSID: ");
  Serial.println(WiFi.SSID());
  IPAddress ip = WiFi.localIP();
  IPAddress gateway = WiFi.gatewayIP();
  Serial.print("IP Address: ");
  Serial.println(ip);


Serial.println(config.path);
Serial.println(config.pubkey);

char full_path [60];
strcpy(full_path, config.path);
strcat(full_path,config.pubkey);

Serial.println(full_path);


//Form the JSON:
  DynamicJsonDocument doc(1024);
  
//  Serial.println(post_url);

  
  doc["private_key"] = config.privkey;
  doc["co2_ppm"] =  co2;
  doc["temperature_c"]=temperature;
  doc["humidity_rh"]=humidity;
  doc["node_id"]=2;
  doc["log"]="OK";
   
  String json;
  serializeJson(doc, json);
  serializeJson(doc, Serial);
  Serial.println("\n");

    String contentType = "application/json";


  http.post(full_path, contentType, json);

  // read the status code and body of the response
  int statusCode = http.responseStatusCode();
  Serial.print("Status code: ");
  Serial.println(statusCode);
  String response = http.responseBody();
  Serial.print("Response: ");
  Serial.println(response);

   u8x8.setFont(u8x8_font_chroma48medium8_r);
  u8x8.setCursor(10,2);
  u8x8.print(statusCode);
  
  //delay(config.interval_sec*1000);
 
             
        
}
