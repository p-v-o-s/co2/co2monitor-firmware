#!/bin/bash

zip -r $1.zip $1
mv $1.zip releases/
git add *
git commit -m 'release $1'
git push
