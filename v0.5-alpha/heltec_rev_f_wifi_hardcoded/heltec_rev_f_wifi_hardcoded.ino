
// *** IMPORTANT: First install required libraries via the Arduino IDE by clicking on each of the links below:
// SCD30:  http://librarymanager/All#SparkFun_SCD30  
// U8g2: http://librarymanager/All#u8x8 
// Bounce2: http://librarymanager/All#bounce2  (install the first library that comes up only)
// ArduinoJSON: http://librarymanager/All#arduinojson
// BMP388_DEV: http://librarymanager/All#bmp388_dev

#include "config.h" // My WiFi configuration.
#include <U8x8lib.h>
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <Wire.h>
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
#include <ArduinoJson.h> //https://arduinojson.org/v6/doc/installation/
#include <BMP388_DEV.h> // https://github.com/MartinL1/BMP388_DEV
#include <Bounce2.h> // https://github.com/thomasfredericks/Bounce2

int NAcounts=0;
int NAthreshold = 10;
boolean connectioWasAlive = true;

WiFiMulti wifiMulti;

SCD30 airSensor;


U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ 15, /* data=*/ 4, /* reset=*/ 16);

long lastMeasureTime = 0;  // the last time the output pin was toggled

long measureDelay = interval_sec*1000;

String post_url="http://"+String(bayou_base_url)+"/"+String(feed_pubkey);

String short_feed_pubkey = String(feed_pubkey).substring(0,3);


void setup() {

  u8x8.begin();
  
  u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("Starting...");
   delay(1000);
   
  
  Serial.begin(115200);
  Serial.println();

  Wire.begin();

  if (airSensor.begin() == false)
  {
     u8x8.setCursor(0,2); 
   u8x8.print("SCD30 missing?");
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }
  u8x8.setCursor(0,2); 
   u8x8.print("SCD30 detected!");
   delay(1000);

   wifiMulti.addAP(SSID,WiFiPassword);
}

int firstLoop = 1;



void loop() {

  if (  ( (millis() - lastMeasureTime) > measureDelay) || firstLoop) {


  
  if (firstLoop) firstLoop = 0;

if (airSensor.dataAvailable())
  {

    int co2 = airSensor.getCO2();
    float temp = roundf(airSensor.getTemperature()* 100) / 100;
    float humid = roundf(airSensor.getHumidity()* 100) / 100;

    float aux_temp = 0.; // dummy value for now
    float aux_press = 0.; //dummy value for now
    
    u8x8.clear();
    //u8x8.setFont(u8x8_font_7x14B_1x2_f);
    //u8x8.setFont(u8x8_font_inr33_3x6_f);
    u8x8.setFont(u8x8_font_inb21_2x4_n);
    u8x8.setCursor(0,0); 
    u8x8.print(co2);
    
    u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(0,5); 
    u8x8.print("Public Key:");
    u8x8.setFont(u8x8_font_7x14B_1x2_f);
    u8x8.setCursor(0,6); 
    u8x8.print(feed_pubkey);
    //u8x8.print(" RH");

    //u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(10,0);
    u8x8.print(short_feed_pubkey);

    //u8x8.setFont(u8x8_font_7x14B_1x2_f);
    u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(10,2);
    u8x8.print("...");
    

while (wifiMulti.run() != WL_CONNECTED && NAcounts < NAthreshold )
{
u8x8.setFont(u8x8_font_chroma48medium8_r);
u8x8.setCursor(10,2);
u8x8.print("wifi?");
u8x8.setCursor(10,3);
u8x8.print(NAcounts);
NAcounts ++;
delay(2000);
}

if (wifiMulti.run() != WL_CONNECTED) {
u8x8.setCursor(8,3);
u8x8.print("RESET");
delay(500);
ESP.restart();
}

        //Form the JSON:
        DynamicJsonDocument doc(1024);
        
        Serial.println(post_url);
        
        doc["private_key"] = feed_privkey;
        doc["co2"] =  co2;
        doc["tempC"]=temp;
        doc["humidity"]=humid;
        doc["mic"]=0.;
        doc["auxPressure"]=aux_press;
        doc["auxTempC"]=aux_temp;
        doc["aux001"]=0.;
        doc["aux002"]=0.;
        doc["aux003"]=0.;
        doc["log"]=0.;
         
        String json;
        serializeJson(doc, json);
        serializeJson(doc, Serial);
        Serial.println("\n");
        
        // Post online

        HTTPClient http;
        int httpCode;
        
        http.begin(post_url);
        http.addHeader("Content-Type", "application/json");
        //Serial.print("[HTTP] Connecting ...\n");
      
        httpCode = http.POST(json);        

        if(httpCode== HTTP_CODE_OK) {
          
            //Serial.printf("[HTTP code]: %d\n", httpCode);

            //u8x8.setFont(u8x8_font_7x14B_1x2_f);
            u8x8.setFont(u8x8_font_chroma48medium8_r);
            u8x8.setCursor(10,2);
            u8x8.print(httpCode);
            
        } else {
            //Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
            //u8x8.setFont(u8x8_font_7x14B_1x2_f);
            u8x8.setFont(u8x8_font_chroma48medium8_r);
              u8x8.setCursor(10,2);
              u8x8.print(httpCode);
              //u8x8.print(http.errorToString(httpCode).c_str());
              u8x8.setCursor(8,3);
              u8x8.print("RESET");
            //Serial.print("reset!");
            delay(5000);
            ESP.restart();
       }

        http.end();
        
  }
    lastMeasureTime = millis(); //set the current time
  }
}
