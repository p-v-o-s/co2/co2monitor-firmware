
// code reference: https://randomnerdtutorials.com/esp32-data-logging-temperature-to-microsd-card/
// schematic reference: https://community.hiveeyes.org/t/heltec-wifi-lora-32/3125
// pins you can use: https://randomnerdtutorials.com/esp32-pinout-reference-gpios/

// *** IMPORTANT: First install required libraries via the Arduino IDE by clicking on each of the links below:
// SCD30:  http://librarymanager/All#SparkFun_SCD30  
// U8g2: http://librarymanager/All#u8x8 
// Bounce2: http://librarymanager/All#bounce2  (install the first library that comes up only)
// ArduinoJSON: http://librarymanager/All#arduinojson
// BMP388_DEV: http://librarymanager/All#bmp388_dev

#include <U8x8lib.h>
#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <Wire.h>
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
#include <ArduinoJson.h> //https://arduinojson.org/v6/doc/installation/
#include <BMP388_DEV.h> // https://github.com/MartinL1/BMP388_DEV
#include <Bounce2.h> // https://github.com/thomasfredericks/Bounce2

// Libraries for SD card
#include "FS.h"
#include "SD.h"
#include <SPI.h>

#define SD_CS 23 // master output
#define SD_SCK 3 // master output
#define SD_MOSI 1 // DO // master output 
#define SD_MISO 39 // DI // master input 

SPIClass sd_spi(HSPI);

    
// Define CS pin for the SD card module
//#define SD_CS 5

const char *SSID = "[Your WiFi SSID]";
const char *WiFiPassword = "[Your WiFi Password]";
const char* bayou_base_url = "data.pvos.org/co2/data/";
const char* feed_pubkey = "[Your Bayou Feed Public Key]";
const char* feed_privkey = "[Your Bayou Feed Private Key]";
const int interval_sec = 10;
const int forcePPM = 400;

WiFiMulti wifiMulti;

SCD30 airSensor;

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ 15, /* data=*/ 4, /* reset=*/ 16);

long lastMeasureTime = 0;  // the last time the output pin was toggled

long measureDelay = interval_sec*1000;

String post_url="http://"+String(bayou_base_url)+"/"+String(feed_pubkey);

String short_feed_pubkey = String(feed_pubkey).substring(0,4);


void setup() {

  u8x8.begin();
  
  u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("Starting...");
   delay(1000);
   
  
  Serial.begin(115200);
  Serial.println();

  Wire.begin();


sd_spi.begin(SD_SCK, SD_MISO, SD_MOSI, SD_CS);
if (!SD.begin(SD_CS, sd_spi)) {
    Serial.println("SD Card: mounting failed.");
     u8x8.setCursor(0,2); 
      u8x8.print("SD mount fail");
}
else{ 
    Serial.println("SD Card: mounted.");
    u8x8.setCursor(0,2); 
      u8x8.print("SD mount!");
}
delay(3000);
/*
// Initialize SD card
  SD.begin(SD_CS);  
  if(!SD.begin(SD_CS)) {
    Serial.println("Card Mount Failed");
    return;
  }
  uint8_t cardType = SD.cardType();
  if(cardType == CARD_NONE) {
    Serial.println("No SD card attached");
    return;
  }
  Serial.println("Initializing SD card...");
  if (!SD.begin(SD_CS)) {
    Serial.println("ERROR - SD card initialization failed!");
    return;    // init failed
  }

  // If the data.txt file doesn't exist
  // Create a file on the SD card and write the data labels
  File file = SD.open("/data.txt");
  if(!file) {
    Serial.println("File doens't exist");
    Serial.println("Creating file...");
    writeFile(SD, "/data.txt", "Reading ID, Date, Hour, Temperature \r\n");
  }
  else {
    Serial.println("File already exists");  
  }
  file.close();
*/

  if (airSensor.begin() == false)
  {
     u8x8.setCursor(0,4); 
   u8x8.print("SCD30 missing?");
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }
  u8x8.setCursor(0,4); 
   u8x8.print("SCD30 detected!");
   delay(1000);

   wifiMulti.addAP(SSID,WiFiPassword);
}

int firstLoop = 1;

void loop() {

  if (  ( (millis() - lastMeasureTime) > measureDelay) || firstLoop) {


  
  if (firstLoop) firstLoop = 0;

if (airSensor.dataAvailable())
  {

    int co2 = airSensor.getCO2();
    float temp = roundf(airSensor.getTemperature()* 100) / 100;
    float humid = roundf(airSensor.getHumidity()* 100) / 100;

    float aux_temp = 0.; // dummy value for now
    float aux_press = 0.; //dummy value for now
    
    u8x8.clear();
    //u8x8.setFont(u8x8_font_7x14B_1x2_f);
    //u8x8.setFont(u8x8_font_inr33_3x6_f);
    u8x8.setFont(u8x8_font_inb21_2x4_n);
    u8x8.setCursor(0,0); 
    u8x8.print(co2);
    
    u8x8.setFont(u8x8_font_7x14B_1x2_f);
    u8x8.setCursor(0,4); 
    u8x8.print(temp);
    u8x8.print(" C");
    u8x8.setCursor(0,6); 
    u8x8.print(humid);
    u8x8.print(" RH");

    //u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(10,0);
    u8x8.print(short_feed_pubkey);

    //u8x8.setFont(u8x8_font_7x14B_1x2_f);
    u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(10,2);
    u8x8.print("...");
    


if((wifiMulti.run() == WL_CONNECTED)) {

        //Form the JSON:
        DynamicJsonDocument doc(1024);
        
        Serial.println(post_url);
        
        doc["private_key"] = feed_privkey;
        doc["co2"] =  co2;
        doc["tempC"]=temp;
        doc["humidity"]=humid;
        doc["mic"]=0.;
        doc["auxPressure"]=aux_press;
        doc["auxTempC"]=aux_temp;
        doc["aux001"]=0.;
        doc["aux002"]=0.;
        doc["aux003"]=0.;
        doc["log"]=0.;
         
        String json;
        serializeJson(doc, json);
        serializeJson(doc, Serial);
        Serial.println("\n");
        
        // Post online

        HTTPClient http;
        int httpCode;
        
        http.begin(post_url);
        http.addHeader("Content-Type", "application/json");
        //Serial.print("[HTTP] Connecting ...\n");
      
        httpCode = http.POST(json);        

        if(httpCode > 0) {
          
            //Serial.printf("[HTTP code]: %d\n", httpCode);

            //u8x8.setFont(u8x8_font_7x14B_1x2_f);
            u8x8.setFont(u8x8_font_chroma48medium8_r);
            u8x8.setCursor(10,2);
            u8x8.print(httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
              
            }
        } else {
            //Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
            //u8x8.setFont(u8x8_font_7x14B_1x2_f);
            u8x8.setFont(u8x8_font_chroma48medium8_r);
              u8x8.setCursor(10,2);
              u8x8.print(http.errorToString(httpCode).c_str());
       }

        http.end();
        
}
else { // no wifi connection
             //u8x8.setFont(u8x8_font_7x14B_1x2_f);
            u8x8.setFont(u8x8_font_chroma48medium8_r);
              u8x8.setCursor(10,2);
              u8x8.print("wifi?");
}
  }
    lastMeasureTime = millis(); //set the current time
  }
}
