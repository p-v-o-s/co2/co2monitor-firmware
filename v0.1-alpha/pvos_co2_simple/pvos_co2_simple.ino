
// Install required libraries via the Arduino IDE by clicking on each of the links below:

// SCD30:  http://librarymanager/All#SparkFun_SCD30  
// U8g2: http://librarymanager/All#u8x8 

#include <U8x8lib.h>

#include <Wire.h>
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
SCD30 airSensor;

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ 15, /* data=*/ 4, /* reset=*/ 16);

long lastMeasureTime = 0;  // the last time the output pin was toggled
long measureDelay = 5000;

void setup() {

  u8x8.begin();
  
  u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
   u8x8.setCursor(0,0); 
   u8x8.print("Starting...");
   delay(1000);
   
  
  Serial.begin(115200);
  Serial.println();

  Wire.begin();

  if (airSensor.begin() == false)
  {
     u8x8.setCursor(0,2); 
   u8x8.print("SCD30 missing?");
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }
  u8x8.setCursor(0,2); 
   u8x8.print("SCD30 detected!");
   delay(1000);
   
}

int firstLoop = 1;

void loop() {

  if (  ( (millis() - lastMeasureTime) > measureDelay) || firstLoop) {
  
  if (firstLoop) firstLoop = 0;

if (airSensor.dataAvailable())
  {

    int co2 = airSensor.getCO2();
    float temp = roundf(airSensor.getTemperature()* 100) / 100;
    float humid = roundf(airSensor.getHumidity()* 100) / 100;

 u8x8.clear();
//u8x8.setFont(u8x8_font_7x14B_1x2_f);
//u8x8.setFont(u8x8_font_inr33_3x6_f);
u8x8.setFont(u8x8_font_inb21_2x4_n);
u8x8.setCursor(0,0); 
u8x8.print(co2);

u8x8.setFont(u8x8_font_7x14B_1x2_f);
u8x8.setCursor(0,4); 
u8x8.print("T: ");
u8x8.print(temp);
u8x8.print(" C");
u8x8.setCursor(0,6); 
u8x8.print("RH: ");
u8x8.print(humid);
u8x8.print(" %");

  }
    lastMeasureTime = millis(); //set the current time
  }
}
