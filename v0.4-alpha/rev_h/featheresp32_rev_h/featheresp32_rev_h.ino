#include <Arduino.h>
#include <WiFi.h>
#include <WiFiMulti.h>
#include <HTTPClient.h>
#include <ArduinoJson.h>
#include <SPI.h>
#include <SD.h>
#include <RH_RF95.h>
#include <RHSoftwareSPI.h>
#include <U8x8lib.h>
#include <Wire.h>
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
#include <Bounce2.h> // https://github.com/thomasfredericks/Bounce2

// SD SPI pins can be chosen freely, but cannot overlap with other ports!

// SDA -- 23
// SCL -- 22


#define SD_CS 33
#define SD_SCK 5
#define SD_MOSI 18
#define SD_MISO 19

#define LORA_IRQ 15
#define LORA_CS 14
#define LORA_SCK 26 //A0
#define LORA_MOSI 21
#define LORA_MISO 25 //A1
#define LORA_RST 27


#define LORA_FREQ 868.0

#define LOG_PATH "/lora_recv.log"

// The sd card can also use a virtual SPI bus
SPIClass sd_spi(HSPI);

// Use a virtual (software) SPI bus for the sx1278
RHSoftwareSPI sx1278_spi;
RH_RF95 rf95(LORA_CS, LORA_IRQ, sx1278_spi);

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ 17, /* data=*/ 16, /* reset=*/ 39);

#define BUTTON_A_PIN 34 
#define BUTTON_B_PIN 36

// INSTANTIATE A Button OBJECT
Button button_A = Button();
Button button_B = Button();

SCD30 airSensor;

WiFiMulti wifiMulti;

int NAcounts=0;
int NAthreshold = 10;
boolean connectioWasAlive = true;


struct Config {
  char wifi_name[30];
  char WiFiPassword[30];
  char bayou_base_url[30];
  char feed_pubkey[30];
  char feed_privkey[30];
  int interval_sec;
  int forcePPM;
};

long lastMeasureTime;  // the last time the output pin was toggled

long measureDelay;

String post_url;

String short_feed_pubkey;

const char *filename = "/config.json";  // <- SD library uses 8.3 filenames
Config config;                         // <- global configuration object

// Loads the configuration from a file
void loadConfiguration(const char *filename, Config &config) {
  // Open file for reading
  File file = SD.open(filename);

  // Allocate a temporary JsonDocument
  // Don't forget to change the capacity to match your requirements.
  // Use arduinojson.org/v6/assistant to compute the capacity.
  StaticJsonDocument<2056> doc;

  // Deserialize the JSON document
  DeserializationError error = deserializeJson(doc, file);
  if (error)
    Serial.println(F("Failed to read file, using default configuration"));


  // Copy values from the JsonDocument to the Config
  //config.port = doc["port"] | 2731;
  strlcpy(config.wifi_name,doc["SSID"],sizeof(config.wifi_name)); 
  strlcpy(config.WiFiPassword,doc["WiFiPassword"],sizeof(config.WiFiPassword));
  strlcpy(config.bayou_base_url,doc["bayou_base_url"],sizeof(config.bayou_base_url));
  strlcpy(config.feed_pubkey,doc["feed_pubkey"],sizeof(config.feed_pubkey));
  strlcpy(config.feed_privkey,doc["feed_privkey"],sizeof(config.feed_privkey));
  config.interval_sec = doc["interval_sec"];
  config.forcePPM = doc["forcePPM"];

  
  // Close the file (Curiously, File's destructor doesn't close the file)

  Serial.println("Loaded configuration:");
  serializeJson(doc, Serial);

  file.close();
}

/*
// Saves the configuration to a file
void saveConfiguration(const char *filename, const Config &config) {
  // Delete existing file, otherwise the configuration is appended to the file
  SD.remove(filename);

  // Open file for writing
  File file = SD.open(filename, FILE_WRITE);
  if (!file) {
    Serial.println(F("Failed to create file"));
    return;
  }

  // Allocate a temporary JsonDocument
  // Don't forget to change the capacity to match your requirements.
  // Use arduinojson.org/assistant to compute the capacity.
  //StaticJsonDocument<256> doc;

  // Set the values in the document
  //doc["hostname"] = config.hostname;
  //doc["port"] = config.port;

  // Serialize JSON to file
  if (serializeJson(doc, file) == 0) {
    Serial.println(F("Failed to write to file"));
  }

  // Close the file
  file.close();
}
*/

// Prints the content of a file to the Serial
void printFile(const char *filename) {
  // Open file for reading
  File file = SD.open(filename);
  if (!file) {
    Serial.println(F("Failed to read file"));
    return;
  }

  // Extract each characters by one by one
  while (file.available()) {
    Serial.print((char)file.read());
  }
  Serial.println();

  // Close the file
  file.close();
}

void setup() {


  Wire.begin();
  
  u8x8.begin();
  
 u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
  u8x8.setCursor(0,0); 
   u8x8.print("Start!");
   delay(1000);
   
    // Builtin LED
    pinMode(LED_BUILTIN, OUTPUT);

    // Serial output
    Serial.begin(115200);

if (airSensor.begin() == false)
  {
    u8x8.setCursor(0,2); 
  u8x8.print("SCD30 missing?");
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }
  u8x8.setCursor(0,2); 
   u8x8.print("SCD30 detected!");
   Serial.println("Air sensor detected!");
   delay(1000);
   
    

    // SD Card
    sd_spi.begin(SD_SCK, SD_MISO, SD_MOSI, SD_CS);


      button_A.attach( BUTTON_A_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_A.interval(5); 
  button_A.setPressedState(LOW);

    button_B.attach( BUTTON_B_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_B.interval(5); 
  button_B.setPressedState(LOW);

  // Initialize SD library
  while (!SD.begin(SD_CS, sd_spi)) {
    Serial.println(F("Failed to initialize SD library"));
    delay(1000);
  }

  // Should load default config if run for the first time
  Serial.println(F("Loading configuration..."));
  loadConfiguration(filename, config);

  // Create configuration file
  //Serial.println(F("Saving configuration..."));
  //saveConfiguration(filename, config);

  // Dump config file
  Serial.println(F("Print config file..."));
  printFile(filename);

  lastMeasureTime = 0;  // the last time the output pin was toggled

  measureDelay = config.interval_sec*1000;

  post_url="http://"+String(config.bayou_base_url)+"/"+String(config.feed_pubkey);

  short_feed_pubkey = String(config.feed_pubkey).substring(0,3);

  wifiMulti.addAP(config.wifi_name,config.WiFiPassword);
  
}

int firstLoop = 1;



void loop() {
    
  button_A.update();
  button_B.update();

   if ( button_A.pressed() ) {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
  
  if ( button_B.pressed() ) {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }

    

  if (  ( (millis() - lastMeasureTime) > measureDelay) || firstLoop) {

  
  if (firstLoop) firstLoop = 0;

if (airSensor.dataAvailable())
  {

    int co2 = airSensor.getCO2();
    float temp = roundf(airSensor.getTemperature()* 100) / 100;
    float humid = roundf(airSensor.getHumidity()* 100) / 100;

    float aux_temp = 0.; // dummy value for now
    float aux_press = 0.; //dummy value for now
    
    u8x8.clear();
    //u8x8.setFont(u8x8_font_7x14B_1x2_f);
    //u8x8.setFont(u8x8_font_inr33_3x6_f);
    u8x8.setFont(u8x8_font_inb21_2x4_n);
    u8x8.setCursor(0,0); 
    u8x8.print(co2);
    
    u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(0,5); 
    u8x8.print("Public Key:");
    u8x8.setFont(u8x8_font_7x14B_1x2_f);
    u8x8.setCursor(0,6); 
    u8x8.print(config.feed_pubkey);
    //u8x8.print(" RH");

    //u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(10,0);
    u8x8.print(short_feed_pubkey);

    //u8x8.setFont(u8x8_font_7x14B_1x2_f);
    u8x8.setFont(u8x8_font_chroma48medium8_r);
    u8x8.setCursor(10,2);
    u8x8.print("...");
    

while (wifiMulti.run() != WL_CONNECTED && NAcounts < NAthreshold )
{
u8x8.setFont(u8x8_font_chroma48medium8_r);
u8x8.setCursor(10,2);
u8x8.print("wifi?");
u8x8.setCursor(10,3);
u8x8.print(NAcounts);
NAcounts ++;
delay(2000);
}

if (wifiMulti.run() != WL_CONNECTED){
u8x8.setCursor(10,4);
Serial.print("reset!");
delay(500);
ESP.restart();
}

        //Form the JSON:
        DynamicJsonDocument doc(1024);
        
        Serial.println(post_url);
        
        doc["private_key"] = config.feed_privkey;
        doc["co2"] =  co2;
        doc["tempC"]=temp;
        doc["humidity"]=humid;
        doc["mic"]=0.;
        doc["auxPressure"]=aux_press;
        doc["auxTempC"]=aux_temp;
        doc["aux001"]=0.;
        doc["aux002"]=0.;
        doc["aux003"]=0.;
        doc["log"]=0.;
         
        String json;
        serializeJson(doc, json);
        serializeJson(doc, Serial);
        Serial.println("\n");
        
        // Post online

        HTTPClient http;
        int httpCode;
        
        http.begin(post_url);
        http.addHeader("Content-Type", "application/json");
        //Serial.print("[HTTP] Connecting ...\n");
      
        httpCode = http.POST(json);        

        if(httpCode > 0) {
          
            //Serial.printf("[HTTP code]: %d\n", httpCode);

            //u8x8.setFont(u8x8_font_7x14B_1x2_f);
            u8x8.setFont(u8x8_font_chroma48medium8_r);
            u8x8.setCursor(10,2);
            u8x8.print(httpCode);

            // file found at server
            if(httpCode == HTTP_CODE_OK) {
                String payload = http.getString();
              
            }
        } else {
            //Serial.printf("[HTTP] GET... failed, error: %s\n", http.errorToString(httpCode).c_str());
            //u8x8.setFont(u8x8_font_7x14B_1x2_f);
            u8x8.setFont(u8x8_font_chroma48medium8_r);
              u8x8.setCursor(10,2);
              u8x8.print(http.errorToString(httpCode).c_str());
       }

        http.end();
        
  }
    lastMeasureTime = millis(); //set the current time
  }
    
    //delay(100);
}
