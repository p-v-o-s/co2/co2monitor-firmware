#include <Arduino.h>
#include <SPI.h>
#include <SD.h>
#include <RH_RF95.h>
#include <RHSoftwareSPI.h>
#include <U8x8lib.h>
#include <Wire.h>
#include "SparkFun_SCD30_Arduino_Library.h"  //  https://github.com/sparkfun/SparkFun_SCD30_Arduino_Library
#include <Bounce2.h> // https://github.com/thomasfredericks/Bounce2

// SD SPI pins can be chosen freely, but cannot overlap with other ports!

// SDA -- 23
// SCL -- 22


#define SD_CS 33
#define SD_SCK 5
#define SD_MOSI 18
#define SD_MISO 19

#define LORA_IRQ 15
#define LORA_CS 14
#define LORA_SCK 26 //A0
#define LORA_MOSI 21
#define LORA_MISO 25 //A1
#define LORA_RST 27


#define LORA_FREQ 868.0

#define LOG_PATH "/lora_recv.log"

// The sd card can also use a virtual SPI bus
SPIClass sd_spi(HSPI);

// Use a virtual (software) SPI bus for the sx1278
RHSoftwareSPI sx1278_spi;
RH_RF95 rf95(LORA_CS, LORA_IRQ, sx1278_spi);

U8X8_SSD1306_128X64_NONAME_SW_I2C u8x8(/* clock=*/ 17, /* data=*/ 16, /* reset=*/ 39);

#define BUTTON_A_PIN 34 
#define BUTTON_B_PIN 36

// INSTANTIATE A Button OBJECT
Button button_A = Button();
Button button_B = Button();

SCD30 airSensor;

void setup() {


  Wire.begin();
  
  u8x8.begin();
  
 u8x8.setFont(u8x8_font_7x14B_1x2_f);
   u8x8.clear();
  u8x8.setCursor(0,0); 
   u8x8.print("Starting...");
   delay(1000);
   
    // Builtin LED
    pinMode(LED_BUILTIN, OUTPUT);

    // Serial output
    Serial.begin(115200);

if (airSensor.begin() == false)
  {
    u8x8.setCursor(0,2); 
  u8x8.print("SCD30 missing?");
    Serial.println("Air sensor not detected. Please check wiring. Freezing...");
    while (1)
      ;
  }
  u8x8.setCursor(0,2); 
   u8x8.print("SCD30 detected!");
   Serial.println("Air sensor detected!");
   delay(1000);
   
    // LoRa: Init
    pinMode(LORA_RST, OUTPUT);
    digitalWrite(LORA_RST, LOW);
    delay(100);
    digitalWrite(LORA_RST, HIGH);

    // the pins for the virtual SPI explicitly to the internal connection
    sx1278_spi.setPins(LORA_MISO, LORA_MOSI, LORA_SCK);

    if (!rf95.init()) 
        Serial.println("LoRa Radio: init failed.");
    else {
        Serial.println("LoRa Radio: init OK!");
        u8x8.setCursor(0,4); 
   u8x8.print("LoRa: okay!");
    }
    // LoRa: set frequency
    if (!rf95.setFrequency(LORA_FREQ))
        Serial.println("LoRa Radio: setFrequency failed.");
    else
        Serial.printf("LoRa Radio: freqency set to %f MHz\n", LORA_FREQ);

    rf95.setModemConfig(RH_RF95::Bw125Cr45Sf128);

    // LoRa: Set max (23 dbm) transmission power. 
    rf95.setTxPower(23, false);

    // SD Card
    sd_spi.begin(SD_SCK, SD_MISO, SD_MOSI, SD_CS);

    if (!SD.begin(SD_CS, sd_spi)) {
        Serial.println("SD Card: mounting failed.");
    }
    else{ 
        Serial.println("SD Card: mounted.");
        delay(1000);
        u8x8.setCursor(0,6); 
        u8x8.print("SD Card mounted!");
    }

      button_A.attach( BUTTON_A_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_A.interval(5); 
  button_A.setPressedState(LOW);

    button_B.attach( BUTTON_B_PIN, INPUT ); // USE EXTERNAL PULL-UP
  button_B.interval(5); 
  button_B.setPressedState(LOW);

  
}

int ledState = LOW;
long lastMeasureTime = 0;  // the last time the output pin was toggled
long measureDelay = 10000;
int firstLoop = 1;

uint8_t lora_buf[RH_RF95_MAX_MESSAGE_LEN];
uint8_t lora_len;
uint8_t receive_counter = 0;

void loop() {
    // Blink LED
    

/*
    lora_len = RH_RF95_MAX_MESSAGE_LEN;
    if (rf95.recv(lora_buf, &lora_len)) {
        receive_counter++;
        Serial.printf("Received LoRa message #%i (%i bytes):\n%s\n", receive_counter, lora_len, lora_buf);

        File test = SD.open(LOG_PATH, FILE_APPEND);
        if (!test) {
            Serial.println("SD Card: writing file failed.");
        } else {
            Serial.printf("SD Card: appending data to %s.\n", LOG_PATH);
            test.write(lora_buf, lora_len);
            test.printf("\n\n");
            test.close();
        }
    }
*/
  button_A.update();
  button_B.update();

   if ( button_A.pressed() ) {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
  
  if ( button_B.pressed() ) {
  digitalWrite(LED_BUILTIN, !digitalRead(LED_BUILTIN));
  }
    
    
    delay(100);
}
